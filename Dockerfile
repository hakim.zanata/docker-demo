FROM debian:stable

# Installation de grafana 

RUN apt update -y && \
    apt install -y wget gnupg apt-transport-https software-properties-common 

RUN wget -O - https://packages.grafana.com/gpg.key | apt-key add -
RUN add-apt-repository "deb https://packages.grafana.com/oss/deb stable main" && \
    apt update
RUN apt update -y && apt-get install -y grafana

#port par defaut

EXPOSE 3000

#CMD

CMD ["/usr/sbin/grafana-server", "-config=/etc/grafana/grafana.ini","-homepath=/usr/share/grafana"]
